# Training Exam on AWS!




## 1.S3 Static Website hosting
### Scenario

<p align="center">
    <img src="https://i.imgur.com/HorqzHq.png" width="60%" height="60%">
</p>

- Region : **US East (N. Virginia)**
 
- You are a solutions architect. Your organization is hosting a static website on s3. 
- The website was working properly with the older version.
- However, when you upload the newer version, something goes wrong. 

- **Try to fix it immediately.**

## Beginners Guide


