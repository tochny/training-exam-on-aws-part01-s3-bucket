AWSTemplateFormatVersion: 2010-09-09
Parameters:
  VpcCIDR:
    Description: Please enter the IP range (CIDR notation) for this VPC
    Type: String
    Default: 10.192.0.0/16
  PublicSubnet1CIDR:
    Description: >-
      Please enter the IP range (CIDR notation) for the public subnet in the
      first Availability Zone
    Type: String
    Default: 10.192.10.0/24
  PublicSubnet2CIDR:
    Description: >-
      Please enter the IP range (CIDR notation) for the public subnet in the
      second Availability Zone
    Type: String
    Default: 10.192.11.0/24
  PrivateSubnet1CIDR:
    Description: >-
      Please enter the IP range (CIDR notation) for the private subnet in the
      first Availability Zone
    Type: String
    Default: 10.192.20.0/24
  PrivateSubnet2CIDR:
    Description: >-
      Please enter the IP range (CIDR notation) for the private subnet in the
      second Availability Zone
    Type: String
    Default: 10.192.21.0/24
Resources:
  VPC:
    Type: 'AWS::EC2::VPC'
    Properties:
      CidrBlock: !Ref VpcCIDR
      EnableDnsSupport: true
      EnableDnsHostnames: true
      Tags:
        - Key: Name
          Value: !Join 
            - '-'
            - - !Ref 'AWS::StackName'
              - VPC
    Metadata:
      'AWS::CloudFormation::Designer':
        id: e56876e8-cec8-4bc1-bad2-9341359cc68d
  InternetGateway:
    Type: 'AWS::EC2::InternetGateway'
    Properties:
      Tags:
        - Key: Name
          Value: !Join 
            - '-'
            - - !Ref 'AWS::StackName'
              - IGW
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 4ca0c9ff-a815-4124-bb1c-3a0ab663eefe
  InternetGatewayAttachment:
    Type: 'AWS::EC2::VPCGatewayAttachment'
    Properties:
      InternetGatewayId: !Ref InternetGateway
      VpcId: !Ref VPC
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 84b235ef-76b2-4233-b992-130a04d95753
  PublicSubnet1:
    Type: 'AWS::EC2::Subnet'
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select 
        - 0
        - !GetAZs ''
      CidrBlock: !Ref PublicSubnet1CIDR
      MapPublicIpOnLaunch: true
      Tags:
        - Key: Name
          Value: !Sub '${AWS::StackName} Public Subnet (AZ1)'
    Metadata:
      'AWS::CloudFormation::Designer':
        id: d436b5ba-7d7a-4af7-9299-5b8576c36d47
  PublicSubnet2:
    Type: 'AWS::EC2::Subnet'
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select 
        - 1
        - !GetAZs ''
      CidrBlock: !Ref PublicSubnet2CIDR
      MapPublicIpOnLaunch: true
      Tags:
        - Key: Name
          Value: !Sub '${AWS::StackName} Public Subnet (AZ2)'
    Metadata:
      'AWS::CloudFormation::Designer':
        id: ce3ec43e-0d62-4bec-babf-2711d01f9e6b
  PrivateSubnet1:
    Type: 'AWS::EC2::Subnet'
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select 
        - 0
        - !GetAZs ''
      CidrBlock: !Ref PrivateSubnet1CIDR
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Sub '${AWS::StackName} Private Subnet (AZ1)'
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 644f7ad2-cf2c-4668-a774-6362c09ce735
  PrivateSubnet2:
    Type: 'AWS::EC2::Subnet'
    Properties:
      VpcId: !Ref VPC
      AvailabilityZone: !Select 
        - 1
        - !GetAZs ''
      CidrBlock: !Ref PrivateSubnet2CIDR
      MapPublicIpOnLaunch: false
      Tags:
        - Key: Name
          Value: !Sub '${AWS::StackName} Private Subnet (AZ2)'
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 6ec0d689-ab09-4fbf-8742-4bfb7416b3c9
  NatGateway1EIP:
    Type: 'AWS::EC2::EIP'
    DependsOn: InternetGatewayAttachment
    Properties:
      Domain: vpc
    Metadata:
      'AWS::CloudFormation::Designer':
        id: ef1358be-d066-40bf-ab13-1f4aeeb6c607
  NatGateway2EIP:
    Type: 'AWS::EC2::EIP'
    DependsOn: InternetGatewayAttachment
    Properties:
      Domain: vpc
    Metadata:
      'AWS::CloudFormation::Designer':
        id: c0ad5ae2-a1e8-4e71-a726-32650d383720
  NatGateway1:
    Type: 'AWS::EC2::NatGateway'
    Properties:
      AllocationId: !GetAtt 
        - NatGateway1EIP
        - AllocationId
      SubnetId: !Ref PublicSubnet1
    Metadata:
      'AWS::CloudFormation::Designer':
        id: e4aabbf8-33a9-4df3-a84c-4f5faa78f182
  NatGateway2:
    Type: 'AWS::EC2::NatGateway'
    Properties:
      AllocationId: !GetAtt 
        - NatGateway2EIP
        - AllocationId
      SubnetId: !Ref PublicSubnet2
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 61985510-a0f2-4b20-9172-89eabd4db6f9
  PublicRouteTable:
    Type: 'AWS::EC2::RouteTable'
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Sub '${AWS::StackName} Public Routes'
    Metadata:
      'AWS::CloudFormation::Designer':
        id: ab3dc175-0443-47b4-8516-9f3fb5ddbff5
  DefaultPublicRoute:
    Type: 'AWS::EC2::Route'
    DependsOn: InternetGatewayAttachment
    Properties:
      RouteTableId: !Ref PublicRouteTable
      DestinationCidrBlock: 0.0.0.0/0
      GatewayId: !Ref InternetGateway
    Metadata:
      'AWS::CloudFormation::Designer':
        id: c38e1303-d8bc-42e8-a468-aaf1c4b0ee1b
  PublicSubnet1RouteTableAssociation:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref PublicSubnet1
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 0961ef32-5eaf-4101-bdaf-8c256230c7cf
  PublicSubnet2RouteTableAssociation:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      RouteTableId: !Ref PublicRouteTable
      SubnetId: !Ref PublicSubnet2
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 7ca51355-56cc-461c-8842-a6aef40f0d64
  PrivateRouteTable1:
    Type: 'AWS::EC2::RouteTable'
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Sub '${AWS::StackName} Private Routes (AZ1)'
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 52371284-c4eb-4628-99d6-c6058256e8fd
  DefaultPrivateRoute1:
    Type: 'AWS::EC2::Route'
    Properties:
      RouteTableId: !Ref PrivateRouteTable1
      DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: !Ref NatGateway1
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 58bb0151-fac1-4e95-98a9-dbb35f543b41
  PrivateSubnet1RouteTableAssociation:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      RouteTableId: !Ref PrivateRouteTable1
      SubnetId: !Ref PrivateSubnet1
    Metadata:
      'AWS::CloudFormation::Designer':
        id: f6882f31-15e2-43a3-8dd0-66e2499442b7
  PrivateRouteTable2:
    Type: 'AWS::EC2::RouteTable'
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Sub '${AWS::StackName} Private Routes (AZ2)'
    Metadata:
      'AWS::CloudFormation::Designer':
        id: e696cc85-d87b-4190-955c-c0f3039621fe
  DefaultPrivateRoute2:
    Type: 'AWS::EC2::Route'
    Properties:
      RouteTableId: !Ref PrivateRouteTable2
      DestinationCidrBlock: 0.0.0.0/0
      NatGatewayId: !Ref NatGateway2
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 26d8cb1c-862c-46d7-a3cb-d16c81aa80fd
  PrivateSubnet2RouteTableAssociation:
    Type: 'AWS::EC2::SubnetRouteTableAssociation'
    Properties:
      RouteTableId: !Ref PrivateRouteTable2
      SubnetId: !Ref PrivateSubnet2
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 31262eed-919e-4a93-b946-ef4e5a790fdb
  NetworkAcl:
    Type: 'AWS::EC2::NetworkAcl'
    Properties:
      VpcId: !Ref VPC
      Tags:
        - Key: Name
          Value: !Join 
            - '-'
            - - !Ref 'AWS::StackName'
              - acl
    Metadata:
      'AWS::CloudFormation::Designer':
        id: c978fdc9-4079-4c87-ba44-b0dcf53beb8b
  InboundHTTPNetworkAclEntry:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      NetworkAclId: !Ref NetworkAcl
      RuleNumber: '100'
      Protocol: '6'
      RuleAction: allow
      Egress: 'false'
      CidrBlock: 0.0.0.0/0
      PortRange:
        From: '80'
        To: '80'
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 34990e1b-e96a-4ba9-befd-44fbff54f485
  InboundSSHNetworkAclEntry:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      NetworkAclId: !Ref NetworkAcl
      RuleNumber: '101'
      Protocol: '6'
      RuleAction: allow
      Egress: 'false'
      CidrBlock: 0.0.0.0/0
      PortRange:
        From: '22'
        To: '22'
    Metadata:
      'AWS::CloudFormation::Designer':
        id: a8568008-9abd-44b6-ae3a-464146ffb7b0
  InboundResponsePortsNetworkAclEntry:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      NetworkAclId: !Ref NetworkAcl
      RuleNumber: '102'
      Protocol: '6'
      RuleAction: allow
      Egress: 'false'
      CidrBlock: 0.0.0.0/0
      PortRange:
        From: '1024'
        To: '65535'
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 9da3f222-97a4-4ed2-8d6a-46efdfee7a34
  OutBoundHTTPNetworkAclEntry:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      NetworkAclId: !Ref NetworkAcl
      RuleNumber: '100'
      Protocol: '6'
      RuleAction: allow
      Egress: 'true'
      CidrBlock: 0.0.0.0/0
      PortRange:
        From: '80'
        To: '80'
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 86b18945-3db7-41f3-9ccd-9aeb8f3d0e42
  OutBoundHTTPSNetworkAclEntry:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      NetworkAclId: !Ref NetworkAcl
      RuleNumber: '101'
      Protocol: '6'
      RuleAction: allow
      Egress: 'true'
      CidrBlock: 0.0.0.0/0
      PortRange:
        From: '443'
        To: '443'
    Metadata:
      'AWS::CloudFormation::Designer':
        id: d6bd12c0-0673-47bc-9724-91ce7b8951bc
  OutBoundResponsePortsNetworkAclEntry:
    Type: 'AWS::EC2::NetworkAclEntry'
    Properties:
      NetworkAclId: !Ref NetworkAcl
      RuleNumber: '102'
      Protocol: '6'
      RuleAction: allow
      Egress: 'true'
      CidrBlock: 0.0.0.0/0
      PortRange:
        From: '1024'
        To: '65535'
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 059a878d-21c6-4e4b-81c5-629ca4bbbe60
  PublicSubnet1NetworkAclAssociation:
    Type: 'AWS::EC2::SubnetNetworkAclAssociation'
    Properties:
      SubnetId: !Ref PublicSubnet1
      NetworkAclId: !Ref NetworkAcl
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 3a626c1f-4e58-4069-8ea7-8d0b45dbe8ad
  PublicSubnet2NetworkAclAssociation:
    Type: 'AWS::EC2::SubnetNetworkAclAssociation'
    Properties:
      SubnetId: !Ref PublicSubnet2
      NetworkAclId: !Ref NetworkAcl
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 3a626c1f-4e58-4069-8ea7-8d0b45dbe8ad
  InstanceSecurityGroup:
    Type: 'AWS::EC2::SecurityGroup'
    Properties:
      VpcId: !Ref VPC
      GroupDescription: Enable http access via port 80
      SecurityGroupIngress:
        - IpProtocol: tcp
          FromPort: '80'
          ToPort: '80'
          CidrIp: 0.0.0.0/0
    Metadata:
      'AWS::CloudFormation::Designer':
        id: d356cff2-bc6e-4900-9c43-05db987af39f
  Bucket:
    Type: 'AWS::S3::Bucket'
    Properties:
      PublicAccessBlockConfiguration:
        BlockPublicAcls: 'false'
        BlockPublicPolicy: 'false'
        IgnorePublicAcls: 'false'
        RestrictPublicBuckets: 'false'
      VersioningConfiguration:
        Status: Enabled
      WebsiteConfiguration:
        IndexDocument: index.html
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 60d6530e-172f-4b3f-98c6-f73469c39073
  BucketPolicy:
    Type: 'AWS::S3::BucketPolicy'
    Properties:
      Bucket: !Ref Bucket
      PolicyDocument:
        Statement:
          - Sid: PublicReadGetObject
            Effect: Allow
            Principal: '*'
            Action: 's3:GetObject'
            Resource: !Sub 'arn:aws:s3:::${Bucket}/*'
    Metadata:
      'AWS::CloudFormation::Designer':
        id: a6be401e-8589-4cc3-b32f-5a67772c9fc5
  AllowEC2AccessS3:
    Type: 'AWS::IAM::Role'
    Properties:
      AssumeRolePolicyDocument:
        Version: 2012-10-17
        Statement:
          - Effect: Allow
            Principal:
              Service:
                - ec2.amazonaws.com
            Action:
              - 'sts:AssumeRole'
      Description: Allow EC2 to access S3 bucket.
      ManagedPolicyArns:
        - 'arn:aws:iam::aws:policy/AmazonS3FullAccess'
        - 'arn:aws:iam::aws:policy/AmazonSSMManagedInstanceCore'
      RoleName: !Sub '${AWS::StackName}-AllowEC2AccessS3'
      Path: "/"
    Metadata:
      'AWS::CloudFormation::Designer':
        id: b8cd3d26-6bd2-4687-9021-37a017395196
  InstanceProfile: 
    Type: "AWS::IAM::InstanceProfile"
    Properties: 
      InstanceProfileName: !Sub '${AWS::StackName}-AllowEC2AccessS3'
      Path: "/"
      Roles:
        - !Ref AllowEC2AccessS3
  Scoreboard:
    Type: 'AWS::EC2::Instance'
    Properties:
      InstanceType: t2.micro
      ImageId: ami-00eb20669e0990cb4
      Tags:
        - Key: Name
          Value: !Join 
            - '-'
            - - !Ref 'AWS::StackName'
              - Scoreboard
      IamInstanceProfile: !Ref InstanceProfile
      UserData: !Base64 
        'Fn::Join':
          - ''
          - - |
              #!/bin/bash -xe
            - |
              sudo yum update -y
            - |
              sudo yum install git -y
            - |
              sudo yum install python36 python36-virtualenv python36-pip -y
            - |
              sudo ln -s /usr/bin/pip-3.6 /usr/bin/pip3
            - |
              sudo pip3 install --upgrade pip
            - |
              sudo ln -s /usr/local/bin/pip3 /usr/bin/pip3
            - |
              sudo pip3 install flask
            - |
              sudo pip3 install requests
            - |
              sudo pip3 install boto3
            - |
              cd /home/ec2-user
            - >
              sudo git clone
              https://gitlab.com/tochny/training-exam-on-aws-part01-s3-bucket.git
            - |
              cd /home/ec2-user/training-exam-on-aws-part01-s3-bucket/Scoreboard
            - 'sudo python3 quizHandler.py '
            - !Ref Bucket
            - |+

            - |
              sudo python3 scoreboard_multi.py
      NetworkInterfaces:
        - SubnetId: !Ref PublicSubnet1
          GroupSet:
            - !Ref InstanceSecurityGroup
          AssociatePublicIpAddress: 'true'
          DeviceIndex: '0'
          DeleteOnTermination: 'true'
    Metadata:
      'AWS::CloudFormation::Designer':
        id: 5fe1c3e7-22c9-4446-907b-2b06e613e55e
    DependsOn:
      - PublicSubnet1
      - InstanceSecurityGroup
Outputs:
  URL:
    Value: !Join 
      - ''
      - - 'http://'
        - !GetAtt 
          - Scoreboard
          - PublicIp
    Description: Scoreboard URL
  WebsiteURL:
    Value: !GetAtt 
      - Bucket
      - WebsiteURL
    Description: URL for website hosted on S3
  VPC:
    Description: A reference to the created VPC
    Value: !Ref VPC
  PublicSubnets:
    Description: A list of the public subnets
    Value: !Join 
      - ','
      - - !Ref PublicSubnet1
        - !Ref PublicSubnet2
  PrivateSubnets:
    Description: A list of the private subnets
    Value: !Join 
      - ','
      - - !Ref PrivateSubnet1
        - !Ref PrivateSubnet2
  PublicSubnet1:
    Description: A reference to the public subnet in the 1st Availability Zone
    Value: !Ref PublicSubnet1
  PublicSubnet2:
    Description: A reference to the public subnet in the 2nd Availability Zone
    Value: !Ref PublicSubnet2
  PrivateSubnet1:
    Description: A reference to the private subnet in the 1st Availability Zone
    Value: !Ref PrivateSubnet1
  PrivateSubnet2:
    Description: A reference to the private subnet in the 2nd Availability Zone
    Value: !Ref PrivateSubnet2
  InstanceSecurityGroup:
    Description: Security group with default rule
    Value: !Ref InstanceSecurityGroup
Metadata:
  'AWS::CloudFormation::Designer':
    b8cd3d26-6bd2-4687-9021-37a017395196:
      size:
        width: 60
        height: 60
      position:
        x: 1170
        'y': 90
      z: 1
      embeds: []
    60d6530e-172f-4b3f-98c6-f73469c39073:
      size:
        width: 60
        height: 60
      position:
        x: 1170
        'y': 210
      z: 1
      embeds: []
    a6be401e-8589-4cc3-b32f-5a67772c9fc5:
      size:
        width: 60
        height: 60
      position:
        x: 1170
        'y': 330
      z: 1
      embeds: []
      isassociatedwith:
        - 60d6530e-172f-4b3f-98c6-f73469c39073
    4ca0c9ff-a815-4124-bb1c-3a0ab663eefe:
      size:
        width: 60
        height: 60
      position:
        x: 1170
        'y': 450
      z: 1
      embeds: []
    e56876e8-cec8-4bc1-bad2-9341359cc68d:
      size:
        width: 1050
        height: 1050
      position:
        x: 60
        'y': 90
      z: 1
      embeds:
        - d356cff2-bc6e-4900-9c43-05db987af39f
        - c978fdc9-4079-4c87-ba44-b0dcf53beb8b
        - e696cc85-d87b-4190-955c-c0f3039621fe
        - 52371284-c4eb-4628-99d6-c6058256e8fd
        - ab3dc175-0443-47b4-8516-9f3fb5ddbff5
        - 6ec0d689-ab09-4fbf-8742-4bfb7416b3c9
        - 644f7ad2-cf2c-4668-a774-6362c09ce735
        - ce3ec43e-0d62-4bec-babf-2711d01f9e6b
        - d436b5ba-7d7a-4af7-9299-5b8576c36d47
    d356cff2-bc6e-4900-9c43-05db987af39f:
      size:
        width: 60
        height: 60
      position:
        x: 300
        'y': 840
      z: 2
      parent: e56876e8-cec8-4bc1-bad2-9341359cc68d
      embeds: []
      iscontainedinside:
        - e56876e8-cec8-4bc1-bad2-9341359cc68d
    c978fdc9-4079-4c87-ba44-b0dcf53beb8b:
      size:
        width: 420
        height: 330
      position:
        x: 90
        'y': 150
      z: 2
      parent: e56876e8-cec8-4bc1-bad2-9341359cc68d
      embeds:
        - 059a878d-21c6-4e4b-81c5-629ca4bbbe60
        - d6bd12c0-0673-47bc-9724-91ce7b8951bc
        - 86b18945-3db7-41f3-9ccd-9aeb8f3d0e42
        - 9da3f222-97a4-4ed2-8d6a-46efdfee7a34
        - a8568008-9abd-44b6-ae3a-464146ffb7b0
        - 34990e1b-e96a-4ba9-befd-44fbff54f485
      iscontainedinside:
        - e56876e8-cec8-4bc1-bad2-9341359cc68d
    059a878d-21c6-4e4b-81c5-629ca4bbbe60:
      size:
        width: 60
        height: 60
      position:
        x: 120
        'y': 210
      z: 3
      parent: c978fdc9-4079-4c87-ba44-b0dcf53beb8b
      embeds: []
      iscontainedinside:
        - c978fdc9-4079-4c87-ba44-b0dcf53beb8b
    d6bd12c0-0673-47bc-9724-91ce7b8951bc:
      size:
        width: 60
        height: 60
      position:
        x: 240
        'y': 210
      z: 3
      parent: c978fdc9-4079-4c87-ba44-b0dcf53beb8b
      embeds: []
      iscontainedinside:
        - c978fdc9-4079-4c87-ba44-b0dcf53beb8b
    86b18945-3db7-41f3-9ccd-9aeb8f3d0e42:
      size:
        width: 60
        height: 60
      position:
        x: 120
        'y': 330
      z: 3
      parent: c978fdc9-4079-4c87-ba44-b0dcf53beb8b
      embeds: []
      iscontainedinside:
        - c978fdc9-4079-4c87-ba44-b0dcf53beb8b
    9da3f222-97a4-4ed2-8d6a-46efdfee7a34:
      size:
        width: 60
        height: 60
      position:
        x: 240
        'y': 330
      z: 3
      parent: c978fdc9-4079-4c87-ba44-b0dcf53beb8b
      embeds: []
      iscontainedinside:
        - c978fdc9-4079-4c87-ba44-b0dcf53beb8b
    a8568008-9abd-44b6-ae3a-464146ffb7b0:
      size:
        width: 60
        height: 60
      position:
        x: 360
        'y': 210
      z: 3
      parent: c978fdc9-4079-4c87-ba44-b0dcf53beb8b
      embeds: []
      iscontainedinside:
        - c978fdc9-4079-4c87-ba44-b0dcf53beb8b
    34990e1b-e96a-4ba9-befd-44fbff54f485:
      size:
        width: 60
        height: 60
      position:
        x: 360
        'y': 330
      z: 3
      parent: c978fdc9-4079-4c87-ba44-b0dcf53beb8b
      embeds: []
      iscontainedinside:
        - c978fdc9-4079-4c87-ba44-b0dcf53beb8b
    e696cc85-d87b-4190-955c-c0f3039621fe:
      size:
        width: 240
        height: 240
      position:
        x: 780
        'y': 750
      z: 2
      parent: e56876e8-cec8-4bc1-bad2-9341359cc68d
      embeds:
        - 26d8cb1c-862c-46d7-a3cb-d16c81aa80fd
      iscontainedinside:
        - e56876e8-cec8-4bc1-bad2-9341359cc68d
    52371284-c4eb-4628-99d6-c6058256e8fd:
      size:
        width: 240
        height: 240
      position:
        x: 780
        'y': 450
      z: 2
      parent: e56876e8-cec8-4bc1-bad2-9341359cc68d
      embeds:
        - 58bb0151-fac1-4e95-98a9-dbb35f543b41
      iscontainedinside:
        - e56876e8-cec8-4bc1-bad2-9341359cc68d
    ab3dc175-0443-47b4-8516-9f3fb5ddbff5:
      size:
        width: 240
        height: 240
      position:
        x: 570
        'y': 150
      z: 2
      parent: e56876e8-cec8-4bc1-bad2-9341359cc68d
      embeds:
        - c38e1303-d8bc-42e8-a468-aaf1c4b0ee1b
      iscontainedinside:
        - e56876e8-cec8-4bc1-bad2-9341359cc68d
    6ec0d689-ab09-4fbf-8742-4bfb7416b3c9:
      size:
        width: 150
        height: 150
      position:
        x: 90
        'y': 840
      z: 2
      parent: e56876e8-cec8-4bc1-bad2-9341359cc68d
      embeds: []
      iscontainedinside:
        - e56876e8-cec8-4bc1-bad2-9341359cc68d
    31262eed-919e-4a93-b946-ef4e5a790fdb:
      source:
        id: e696cc85-d87b-4190-955c-c0f3039621fe
      target:
        id: 6ec0d689-ab09-4fbf-8742-4bfb7416b3c9
    644f7ad2-cf2c-4668-a774-6362c09ce735:
      size:
        width: 150
        height: 150
      position:
        x: 870
        'y': 150
      z: 2
      parent: e56876e8-cec8-4bc1-bad2-9341359cc68d
      embeds: []
      iscontainedinside:
        - e56876e8-cec8-4bc1-bad2-9341359cc68d
    f6882f31-15e2-43a3-8dd0-66e2499442b7:
      source:
        id: 52371284-c4eb-4628-99d6-c6058256e8fd
      target:
        id: 644f7ad2-cf2c-4668-a774-6362c09ce735
    ce3ec43e-0d62-4bec-babf-2711d01f9e6b:
      size:
        width: 240
        height: 240
      position:
        x: 480
        'y': 540
      z: 2
      parent: e56876e8-cec8-4bc1-bad2-9341359cc68d
      embeds:
        - 61985510-a0f2-4b20-9172-89eabd4db6f9
      iscontainedinside:
        - e56876e8-cec8-4bc1-bad2-9341359cc68d
    3a626c1f-4e58-4069-8ea7-8d0b45dbe8ad:
      source:
        id: c978fdc9-4079-4c87-ba44-b0dcf53beb8b
      target:
        id: d436b5ba-7d7a-4af7-9299-5b8576c36d47
    7ca51355-56cc-461c-8842-a6aef40f0d64:
      source:
        id: ab3dc175-0443-47b4-8516-9f3fb5ddbff5
      target:
        id: ce3ec43e-0d62-4bec-babf-2711d01f9e6b
    d436b5ba-7d7a-4af7-9299-5b8576c36d47:
      size:
        width: 330
        height: 240
      position:
        x: 90
        'y': 540
      z: 2
      parent: e56876e8-cec8-4bc1-bad2-9341359cc68d
      embeds:
        - 5fe1c3e7-22c9-4446-907b-2b06e613e55e
        - e4aabbf8-33a9-4df3-a84c-4f5faa78f182
      iscontainedinside:
        - e56876e8-cec8-4bc1-bad2-9341359cc68d
    0961ef32-5eaf-4101-bdaf-8c256230c7cf:
      source:
        id: ab3dc175-0443-47b4-8516-9f3fb5ddbff5
      target:
        id: d436b5ba-7d7a-4af7-9299-5b8576c36d47
    84b235ef-76b2-4233-b992-130a04d95753:
      source:
        id: e56876e8-cec8-4bc1-bad2-9341359cc68d
      target:
        id: 4ca0c9ff-a815-4124-bb1c-3a0ab663eefe
    5fe1c3e7-22c9-4446-907b-2b06e613e55e:
      size:
        width: 60
        height: 60
      position:
        x: 120
        'y': 600
      z: 3
      parent: d436b5ba-7d7a-4af7-9299-5b8576c36d47
      embeds: []
      iscontainedinside:
        - d436b5ba-7d7a-4af7-9299-5b8576c36d47
      dependson:
        - 84b235ef-76b2-4233-b992-130a04d95753
    c38e1303-d8bc-42e8-a468-aaf1c4b0ee1b:
      size:
        width: 60
        height: 60
      position:
        x: 600
        'y': 210
      z: 3
      parent: ab3dc175-0443-47b4-8516-9f3fb5ddbff5
      embeds: []
      isassociatedwith:
        - 4ca0c9ff-a815-4124-bb1c-3a0ab663eefe
      iscontainedinside:
        - ab3dc175-0443-47b4-8516-9f3fb5ddbff5
      dependson:
        - 84b235ef-76b2-4233-b992-130a04d95753
    c0ad5ae2-a1e8-4e71-a726-32650d383720:
      size:
        width: 60
        height: 60
      position:
        x: 1170
        'y': 570
      z: 1
      embeds: []
      dependson:
        - 84b235ef-76b2-4233-b992-130a04d95753
    61985510-a0f2-4b20-9172-89eabd4db6f9:
      size:
        width: 60
        height: 60
      position:
        x: 510
        'y': 600
      z: 3
      parent: ce3ec43e-0d62-4bec-babf-2711d01f9e6b
      embeds: []
      iscontainedinside:
        - ce3ec43e-0d62-4bec-babf-2711d01f9e6b
    26d8cb1c-862c-46d7-a3cb-d16c81aa80fd:
      size:
        width: 60
        height: 60
      position:
        x: 810
        'y': 810
      z: 3
      parent: e696cc85-d87b-4190-955c-c0f3039621fe
      embeds: []
      isassociatedwith:
        - 61985510-a0f2-4b20-9172-89eabd4db6f9
      iscontainedinside:
        - e696cc85-d87b-4190-955c-c0f3039621fe
    ef1358be-d066-40bf-ab13-1f4aeeb6c607:
      size:
        width: 60
        height: 60
      position:
        x: 1170
        'y': 690
      z: 1
      embeds: []
      dependson:
        - 84b235ef-76b2-4233-b992-130a04d95753
    e4aabbf8-33a9-4df3-a84c-4f5faa78f182:
      size:
        width: 60
        height: 60
      position:
        x: 240
        'y': 600
      z: 3
      parent: d436b5ba-7d7a-4af7-9299-5b8576c36d47
      embeds: []
      iscontainedinside:
        - d436b5ba-7d7a-4af7-9299-5b8576c36d47
    58bb0151-fac1-4e95-98a9-dbb35f543b41:
      size:
        width: 60
        height: 60
      position:
        x: 810
        'y': 510
      z: 3
      parent: 52371284-c4eb-4628-99d6-c6058256e8fd
      embeds: []
      isassociatedwith:
        - e4aabbf8-33a9-4df3-a84c-4f5faa78f182
      iscontainedinside:
        - 52371284-c4eb-4628-99d6-c6058256e8fd
